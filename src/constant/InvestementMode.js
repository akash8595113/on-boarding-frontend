const InvestementMode = [
  {
    id: 1,
    investement_mode: "Direct equity",
  },
  {
    id: 2,
    investement_mode: "Equity mutual funds",
  },
  {
    id: 3,
    investement_mode: "Debt mutual funds",
  },
  {
    id: 4,
    investement_mode: "Public Provident Fund (PPF)",
  },
  {
    id: 5,
    investement_mode: "Bank fixed deposit (FD)",
  },
];

export default InvestementMode;
