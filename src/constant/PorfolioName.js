const PortfolioName = [
  {
    id: 1,
    portfolio_name: "Active Portfolio Strategies",
  },
  {
    id: 2,
    portfolio_name: "Passive Strategies",
  },
  {
    id: 3,
    portfolio_name: "Aggressive Strategies",
  },
  {
    id: 4,
    portfolio_name: "Defensive Strategies",
  },
];

export default PortfolioName;
