import { createTheme, responsiveFontSizes } from "@mui/material/styles";

export const primary = {
  main: "#1EA596",
  lightMain: "#0E4059",

  mainText: "#000000",
  secondaryText: "#404040",
  lightText: "#FFFFFF",
};

const fontFamilyInfo = `'Inter', sans-serif`;

let themeDef = createTheme({
  typography: {
    fontFamily: fontFamilyInfo,
    color: primary.mainText,
    allVariants: {},
  },

  palette: {
    primary: primary,
  },

  MuiSelect: {
    defaultProps: {
      MenuProps: {
        style: {
          maxHeight: 200,
          fontFamily: fontFamilyInfo,
        },
      },
    },
  },

  MuiOutlinedInput: { 
    styleOverrides: {
      root: {
        borderRadius: 8,
        fontFamily: fontFamilyInfo,
      },
    },
  },

  components: {
    MuiFormControlLabel: {
      styleOverrides: {
        root: {
          border: `1px solid #1EA596`,
          borderRadius: "6px",
          padding: "2px 8px",
          margin: "0px 8px",
          color: "#1EA596",
        },
      },
    },
  },
  // overrides:{
  //   MuiFormControlLabel:{
  //     root:{
  //       // fontSize: 50
  //       fontWeight: 500,
  //       color:'red'
  //     }
  //   }
  // },
});

export const theme = responsiveFontSizes(themeDef);
