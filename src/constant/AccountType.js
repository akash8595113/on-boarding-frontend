const AccountType = [
  {
    id: 1,
    account_type: "Savings Account",
  },
  {
    id: 2,
    account_type: "Current Account",
  },
  {
    id: 3,
    account_type: "Salary Account",
  },
  {
    id: 4,
    account_type: "Fixed Deposite Account",
  },
  {
    id: 5,
    account_type: "Recurring Deposite Account",
  },
];

export default AccountType;
