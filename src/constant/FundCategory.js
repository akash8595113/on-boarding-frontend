const FundCategory = [
  {
    id: 1,
    fund_category: "Redemption Fee",
  },
  {
    id: 2,
    fund_category: "Exchange Fee",
  },
  {
    id: 3,
    fund_category: "Account Fee",
  },
  {
    id: 4,
    fund_category: "Purchase  Fee",
  },
];

export default FundCategory;
