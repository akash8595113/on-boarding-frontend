import { Box, styled, Typography } from "@mui/material";
import React from "react";
import ReactDatePicker from "react-datepicker";

const DatePickerStyle = styled(Box)(({ theme }) => ({
  ".dobTextStyle": {
    fontWeight: 400,
    fontSize: "24px",
    padding: "4px",
  },
  ".datePicker": {
    width: "100%",
    padding: "20px",
    margin: "8px 0",
    border: "1px solid #ccc",
    borderRadius: "4px",
  },
}));

const DatePicker = ({ name, value, handleChange }) => {

  return (
    <DatePickerStyle>
      <Typography className="dobTextStyle">Date Of Birth</Typography>
      <ReactDatePicker
        className="datePicker"
        name={name}
        placeholderText="DD/MM/YYYY"
        dateFormat="dd/MM/yyyy"
        selected={(value && new Date(value)) || null}
        onChange={(val) => {
          handleChange(name, val);
        }}
      />
    </DatePickerStyle>
  );
};

export default DatePicker;
