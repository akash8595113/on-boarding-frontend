import { ThemeProvider } from "@mui/system";
import React from "react";
import ApplicantsDetails from "./components/ApplicantsDetails";
import BankDetails from "./components/BankDetails";
import InvestementDetails from "./components/InvestementDetails";
import InvestementRiskInfo from "./components/InvestementRiskInfo";
import NomineeDetails from "./components/NomineeDetails";
import { theme } from "./constant/theme";
import { useFormik } from "formik";

function App() {
  const formik = useFormik({
    initialValues: {
      account_type: "",
      strategy_name: "",
      fees_category: "",
      investment_category: "",
      address: "",
      country: "",
      count_account_holders: "",
      quantum_investment: "",
      investment_mode: "",
      applicant_name: "",
      applicant_contact_number: "",
      applicant_email_id: "",
      applicant_pan_no: "",
      applicant_dob: "",
      bank_preference: "",
      bank_name: "",
      account_no: "",
      account_title: "",
      IFSC_code: "",
      count_nominee: "",
      nominee_name: "",
      nominee_pan: "",
      nominee_email_id: "",
      nominee_contact_no: "",
      nominee_dob: "",
      nominee_relation: "",
      nominee_per: "",
      investment_experience: "",
      investment_style: "",
      reaction_on_portfolio_diversified: "",
      investment_objective: "",
      risk_tolerance: "",
      investment_horizon: "",
    },

    onSubmit: async (values) => {
      const response = await fetch("http://52.66.246.79:8080/api", {
        method: "POST",
        body: JSON.stringify(values),
        headers: {
          "Content-Type": "application/json",
        },
      });
      const data = await response.text();
      console.log(data);
    },
  });
  return ( 
    <ThemeProvider theme={theme}>
      <form onSubmit={formik.handleSubmit}>
        <InvestementDetails formik={formik} />
        <ApplicantsDetails formik={formik} />
        <BankDetails formik={formik} />
        <NomineeDetails formik={formik} />
        <InvestementRiskInfo formik={formik} />
      </form>
    </ThemeProvider>
  );
}

export default App;
