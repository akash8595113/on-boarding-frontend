import {
  Box,
  Button,
  Divider,
  styled,
  TextField,
  Typography,
} from "@mui/material";
import React from "react";
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "../constant/DatePicker";

const ApplicantDetailsStyle = styled(Box)(({ theme }) => ({
  margin: "4rem",
  ".container": {
    display: "flex",
  },
  ".heading1": {
    backgroundColor: theme.palette.primary.lightMain,
    fontWeight: 600,
    color: theme.palette.primary.lightText,
    padding: "6px",
    margin: "10px",
    fontSize: "24px",
  },
  ".box1": {
    width: "18%",
  },
  ".box2": {
    width: "80%",
  },
  ".container1": {
    display: "flex",
    margin: "4px",
  },
  ".inputStyle": {
    margin: "12px",
    width: "98%",
  },
  ".btnStyle": {
    width: "15%",
    margin: "1rem",
    border: `2px solid theme.palette.primary.main`,
    borderRadius: "6px",
    textTransform: "capitalize",
    fontSize: "1.2rem",
  },
  ".reSendBtn": {
    width: "15%",
    color: theme.palette.primary.lightMain,
    margin: "1rem",
    textTransform: "capitalize",
    fontSize: "1rem",
  },

  //
  ".uploadFiledContainer": {
    display: "flex",
    margin: "2rem",
  },
  ".uploadFileText": {
    fontWeight: 500,
    fontSize: "24px",
    margin: "2px 1.5rem",
    color: theme.palette.primary.lightMain,
  },
  ".uploadBtnFile": {
    textTransform: "capitalize",
    fontSize: "1.2rem",
    margin: "5px 20px",
    padding: "5px 25px",
    borderRadius: "6px",
  },
}));

const ApplicantsDetails = ({ formik }) => {

  
  return (
    <ApplicantDetailsStyle>
      <Box className="container">
        <Box className="box1">
          <Typography className="heading1">Applicant Details</Typography>
        </Box>
        <Box className="box2">
          <Typography className="heading1">
            1st Account Holder Details
          </Typography>
          <TextField
            fullWidth={true}
            name="applicant_name"
            label="Applicant Name"
            className="inputStyle"
            value={formik.values.applicant_name}
            onChange={formik.handleChange}
          />
          <div className="container1">
            <TextField
              fullWidth={true}
              label="Contact No."
              className="inputStyle"
              name="applicant_contact_number"
              value={formik.values.applicant_contact_number}
              onChange={formik.handleChange}
            />
            <Button variant="outlined" className="btnStyle">
              Send OTP
            </Button>
          </div>
          <div className="container1">
            <TextField
              fullWidth={true}
              label="Enter OTP"
              className="inputStyle"
            />
            <Button variant="contained" className="btnStyle">
              Verify
            </Button>
            <Button className="reSendBtn">Re-Send OTP</Button>
          </div>

          <div className="container1">
            <TextField
              fullWidth={true}
              label="Email Id"
              className="inputStyle"
              name="applicant_email_id"
              value={formik.values.applicant_email_id}
              onChange={formik.handleChange}
            />
            <Button variant="outlined" className="btnStyle">
              Send OTP
            </Button>
          </div>
          <div className="container1">
            <TextField
              fullWidth={true}
              label="Enter OTP"
              className="inputStyle"
            />
            <Button variant="contained" className="btnStyle">
              Verify
            </Button>
            <Button className="reSendBtn">Re-Send OTP</Button>
          </div>

          <TextField
            fullWidth={true}
            label="PAN No."
            className="inputStyle"
            name="applicant_pan_no"
            value={formik.values.applicant_pan_no}
            onChange={formik.handleChange}
          />

          <Box margin={2}>
            <DatePicker
              name="applicant_dob"
              value={formik.values.applicant_dob}
              handleChange={formik.setFieldValue}
            />
          </Box>

          <Box className="uploadFiledContainer">
            <Typography className="uploadFileText">
              Upload scanned copy of PAN Card
            </Typography>
            <label htmlFor="upload-photo">
              <input
                style={{ display: "none" }}
                id="upload-photo"
                name="upload-photo"
                type="file"
              />
              <Button
                variant="outlined"
                component="span"
                className="uploadBtnFile"
              >
                Choose File
              </Button>{" "}
            </label>
            <Button variant="contained" className="uploadBtnFile">
              Upload
            </Button>
          </Box>

          <Box className="uploadFiledContainer">
            <Typography className="uploadFileText">
              Upload scanned copy of Aadhar Card
            </Typography>
            <label htmlFor="upload-photo">
              <input
                style={{ display: "none" }}
                id="upload-photo"
                name="upload-photo"
                type="file"
              />
              <Button
                variant="outlined"
                component="span"
                className="uploadBtnFile"
              >
                Choose File
              </Button>{" "}
            </label>
            <Button variant="contained" className="uploadBtnFile">
              Upload
            </Button>
          </Box>
        </Box>
      </Box>
      <Divider />
    </ApplicantDetailsStyle>
  );
};

export default ApplicantsDetails;
