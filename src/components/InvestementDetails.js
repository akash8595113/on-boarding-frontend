import {
  Box,
  Divider,
  MenuItem,
  Select,
  styled,
  TextField,
  Typography,
} from "@mui/material";
import React from "react";
import AccountType from "../constant/AccountType";
import FundCategory from "../constant/FundCategory";
import InvestementMode from "../constant/InvestementMode";
import PortfolioName from "../constant/PorfolioName";
import countries from "i18n-iso-countries";
import enLocale from "i18n-iso-countries/langs/en.json";
import itLocale from "i18n-iso-countries/langs/it.json";

const InvestementDetailsStyle = styled(Box)(({ theme }) => ({
  margin: "4rem",
  ".heading": {
    color: theme.palette.primary.lightMain,
    fontWeight: 400,
    fontSize: "24px",
    textAlign: "center",
  },
  ".subHeading1": {
    color: theme.palette.primary.main,
    fontWeight: 600,
    fontSize: "24px",
  },
  ".box1": {
    backgroundColor: theme.palette.primary.lightMain,
    color: theme.palette.primary.lightText,
    margin: "3rem 1rem",
    padding: "4px",
  },
  ".selectContainer1": {
    padding: "1rem",
    display: "flex",
    flexDirection: "row",
  },
  ".labelBox": {
    width: "30%",
  },
  ".label": {
    fontSize: "20px",
    textAlign: "start",
  },
  ".subLabel": {
    width: "90%",
    fontWeight: 300,
    fontSize: "12px",
    color: theme.palette.primary.secondaryText,
  },
}));

const countAccountHolder = [1, 2, 3,  4];

const InvestementDetails = ({ formik }) => {
  // Have to register the languages you want to use
  countries.registerLocale(enLocale);
  countries.registerLocale(itLocale);

  // Returns an object not a list
  const countryObj = countries.getNames("en", { select: "official" });

  const countryArr = Object.entries(countryObj).map(([key, value]) => {
    return {
      label: value,
      value: key,
    };
  });

  return (
    <InvestementDetailsStyle>
      <Typography className="heading">
        Welcome Investor,
        <br />
        <span className="subHeading1">
          "To a World of Sustainable Wealth Creation"
        </span>
      </Typography>
      <Typography className="heading">
        A Financial Journey filled with capitalization of enormous wealth
        opportunity in an economic transition.
      </Typography>
      <Box className="box1">
        <Typography sx={{ fontSize: "28px", fontWeight: 500 }}>
          Investment Details
        </Typography>
      </Box>

      <Box className="selectContainer1">
        <Box className="labelBox">
          <Typography className="label">Account Type</Typography>
        </Box>
        <Select
          name="account_type"
          displayEmpty
          fullWidth={true}
          value={formik.values.account_type}
          onChange={formik.handleChange}
        >
          <MenuItem disabled value="">
            Select
          </MenuItem>
          {AccountType.map((account) => (
            <MenuItem key={account.id} value={account.account_type}>
              {account.account_type}
            </MenuItem>
          ))}
        </Select>
      </Box>

      <Box className="selectContainer1">
        <Box className="labelBox">
          <Typography className="label">Portfolio/Strategy Name</Typography>
        </Box>
        <Select
          displayEmpty
          name="strategy_name"
          fullWidth={true}
          value={formik.values.strategy_name}
          onChange={formik.handleChange}
        >
          <MenuItem disabled value="">
            Select
          </MenuItem>
          {PortfolioName.map((portfolio) => (
            <MenuItem key={portfolio.id} value={portfolio.portfolio_name}>
              {portfolio.portfolio_name}
            </MenuItem>
          ))}
        </Select>
      </Box>

      <Box className="selectContainer1">
        <Box className="labelBox">
          <Typography className="label">Fund Fees Category</Typography>
          <Typography className="subLabel">
            Fees preview according to selection
          </Typography>
        </Box>
        <Select
          name="fees_category"
          displayEmpty
          fullWidth={true}
          size="small"
          value={formik.values.fees_category}
          onChange={formik.handleChange}
        >
          <MenuItem disabled value="">
            Select
          </MenuItem>
          {FundCategory.map((fund) => (
            <MenuItem key={fund.id} value={fund.fund_category}>
              {fund.fund_category}
            </MenuItem>
          ))}
        </Select>
      </Box>

      <Box className="selectContainer1">
        <Box className="labelBox">
          <Typography className="label">Investment Category</Typography>
          <Typography className="subLabel">
            Preview only when systematic transfer plan is selected
          </Typography>
        </Box>
        <Select
          name="investment_category"
          displayEmpty
          fullWidth={true}
          size="small"
          value={formik.values.investment_category}
          onChange={formik.handleChange}
        >
          <MenuItem disabled value="">
            Select
          </MenuItem>
          {FundCategory.map((fund) => (
            <MenuItem key={fund.id} value={fund.fund_category}>
              {fund.fund_category}
            </MenuItem>
          ))}
        </Select>
      </Box>

      <Box className="selectContainer1">
        <Box className="labelBox">
          <Typography className="label">Communication Address</Typography>
          <Typography className="subLabel">
            (All further deliverables will be communicated on this address)
          </Typography>
        </Box>
        <TextField
          fullWidth={true}
          type="text"
          name="address"
          value={formik.values.address}
          onChange={formik.handleChange}
        />
      </Box>

      <Box className="selectContainer1">
        <Box className="labelBox">
          <Typography className="label">Country of Tax Residency </Typography>
        </Box>
        <Select
          displayEmpty
          fullWidth={true}
          name="country"
          value={formik.values.country}
          onChange={formik.handleChange}
        >
          <MenuItem disabled value="">
            Select
          </MenuItem>
          {!!countryArr?.length &&
            countryArr.map(({ label, value }) => (
              <MenuItem key={value} value={value}>
                {label}
              </MenuItem>
            ))}
        </Select>
      </Box>

      <Box className="selectContainer1">
        <Box className="labelBox">
          <Typography className="label">No. of Account Holders</Typography>
        </Box>
        <Select
          name="count_account_holders"
          displayEmpty
          fullWidth={true}
          value={formik.values.count_account_holders}
          onChange={formik.handleChange}
        >
          <MenuItem disabled value="">
            Select
          </MenuItem>
          {countAccountHolder.map((countAccount) => (
            <MenuItem key={countAccount.toString()} value={countAccount}>
              {countAccount}
            </MenuItem>
          ))}
        </Select>
      </Box>

      <Box className="selectContainer1">
        <Box className="labelBox">
          <Typography className="label">Quantum of Investment</Typography>
        </Box>
        <TextField
          type="text"
          name="quantum_investment"
          fullWidth={true}
          value={formik.values.quantum_investments}
          onChange={formik.handleChange}
        />
      </Box>

      <Box className="selectContainer1">
        <Box className="labelBox">
          <Typography className="label">Investment Mode</Typography>
        </Box>
        <Select
          name=" investment_mode"
          displayEmpty
          fullWidth={true}
          value={formik.values.investment_mode}
          onChange={formik.handleChange}
        >
          <MenuItem disabled value="">
            Select
          </MenuItem>
          {InvestementMode.map((investement) => (
            <MenuItem key={investement.id} value={investement.investement_mode}>
              {investement.investement_mode}
            </MenuItem>
          ))}
        </Select>
      </Box>
      <Divider />
    </InvestementDetailsStyle>
  );
};

export default InvestementDetails;
