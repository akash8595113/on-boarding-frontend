import {
  Box,
  MenuItem,
  Select,
  styled,
  TextField,
  Typography,
} from "@mui/material";
import React from "react";
import DatePicker from "../constant/DatePicker";

const NomineeStyle = styled(Box)(({ theme }) => ({
  margin: "4rem",
  ".box1": {
    width: "20%",
    margin: "1rem 4px",
  },
  ".box2": {
    width: "74%",
    margin: "2px 2rem",
  },
  ".heading": {
    backgroundColor: theme.palette.primary.lightMain,
    fontWeight: 600,
    color: theme.palette.primary.lightText,
    padding: "6px",
    margin: "10px",
    fontSize: "24px",
  },
  ".textContainer": {
    width: "100%",
    backgroundColor: theme.palette.primary.lightMain,
    fontWeight: 600,
    color: theme.palette.primary.lightText,
    padding: "6px",
    margin: "1.2rem",
    fontSize: "24px",
  },
  ".inputStyle": {
    margin: "1.5rem",
  },
}));

const countNominee = [1, 2, 3, 4];

const NomineeDetails = ({ formik }) => {
  return (
    <NomineeStyle>
      <Box display={"flex"}>
        <Box className="box1">
          <Typography className="heading">Nominee Details</Typography>
        </Box>
        <Box className="box2">
          <Select
            className="inputStyle"
            name="count_nominee"
            displayEmpty
            fullWidth={true}
            value={formik.values.count_nominee}
            onChange={formik.handleChange}
          >
            <MenuItem disabled value="">
              Select
            </MenuItem>
            {countNominee.map((count) => (
              <MenuItem key={count} value={count}>
                {count}
              </MenuItem>
            ))}
          </Select>
          <Box>
            <Typography className="textContainer">
              1sr Nominee Details
            </Typography>
          </Box>
          <TextField
            fullWidth={true}
            label="Nominee Name"
            className="inputStyle"
            name="nominee_name"
            value={formik.values.nominee_name}
            onChange={formik.handleChange}
          />
          <TextField
            fullWidth={true}
            label="Nominee PAN"
            className="inputStyle"
            name="nominee_pan"
            value={formik.values.nominee_pan}
            onChange={formik.handleChange}
          />
          <TextField
            fullWidth={true}
            label="Nominee Email Id."
            className="inputStyle"
            name="nominee_email_id"
            value={formik.values.nominee_email_id}
            onChange={formik.handleChange}
          />
          <TextField
            fullWidth={true}
            label="Nominee Contact No."
            className="inputStyle"
            name="nominee_contact_no"
            value={formik.values.nominee_contact_no}
            onChange={formik.handleChange}
          />

          <Box margin={2}>
            <DatePicker
              name="nominee_dob"
              value={formik.values.nominee_dob}
              handleChange={formik.setFieldValue}
            />
          </Box>

          <TextField
            fullWidth={true}
            label="Nominee Relationship with 1st Applicant"
            className="inputStyle"
            name="nominee_relation"
            value={formik.values.nominee_relation}
            onChange={formik.handleChange}
          />

          <TextField
            fullWidth={true}
            label="Nominee (%)"
            className="inputStyle"
            name="nominee_per"
            value={formik.values.nominee_per}
            onChange={formik.handleChange}
          />
        </Box>
      </Box>
    </NomineeStyle>
  );
};

export default NomineeDetails;
