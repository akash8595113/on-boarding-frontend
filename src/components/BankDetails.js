import {
  Box,
  FormControlLabel,
  Radio,
  RadioGroup,
  styled,
  TextField,
  Typography,
} from "@mui/material";
import React from "react";

const BankDetailsStyle = styled(Box)(({ theme }) => ({
  margin: "4rem",
  ".heading": {
    backgroundColor: theme.palette.primary.lightMain,
    fontWeight: 600,
    color: theme.palette.primary.lightText,
    padding: "6px",
    margin: "10px",
    fontSize: "24px",
  },
  ".box1": {
    width: "20%",
  },
  ".box2": {
    width: "75%",
  },
  ".radioGroupContainer": {
    display: "flex",
    margin: "1rem 2rem",
  },
  ".inputStyle": {
    margin: "1.5rem",
  },
}));

const BankDetails = ({ formik }) => {
  return (
    <BankDetailsStyle>
      <Box sx={{ display: "flex" }}>
        <div className="box1">
          <Typography className="heading">Banking Preferences</Typography>
        </div>
        <Box className="box2">
          <RadioGroup
            name="bank_preference"
            defaultValue="Bank Details"
            value={formik.values.bank_preference}
            onChange={formik.handleChange}
          >
            <Box className="radioGroupContainer">
              <FormControlLabel
                control={<Radio />}
                label="Cancelled Cheque"
                disabled
              />
              <FormControlLabel
                value="Bank Details"
                control={<Radio />}
                label="Bank Details"
              />
            </Box>
          </RadioGroup>

          <TextField
            fullWidth={true}
            label="Bank Name"
            className="inputStyle"
            name="bank_name"
            value={formik.values.bank_name}
            onChange={formik.handleChange}
          />

          <TextField
            fullWidth={true}
            label="Account No."
            className="inputStyle"
            name="account_no"
            value={formik.values.account_no}
            onChange={formik.handleChange}
          />

          <TextField
            fullWidth={true}
            label="Account Title"
            className="inputStyle"
            name="account_title"
            value={formik.values.account_title}
            onChange={formik.handleChange}
          />

          <TextField
            fullWidth={true}
            label="Bank IFSC Code"
            className="inputStyle"
            name="IFSC_code"
            value={formik.values.IFSC_code}
            onChange={formik.handleChange}
          />
        </Box>
      </Box>
    </BankDetailsStyle>
  );
};

export default BankDetails;
