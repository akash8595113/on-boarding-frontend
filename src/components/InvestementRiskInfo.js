import {
  Box,
  Button,
  FormControlLabel,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  styled,
  Typography,
} from "@mui/material";
import React from "react";

const InsvementRiskStyle = styled(Box)(({ theme }) => ({
  margin: "4rem",
  ".box1": {
    backgroundColor: theme.palette.primary.lightMain,
    color: theme.palette.primary.lightText,
    margin: "3rem 1rem",
    padding: "4px",
  },
  ".heading": {
    color: theme.palette.primary.lightMain,
    fontWeight: 400,
    fontSize: "24px",
    textAlign: "center",
    margin: "3rem ",
  },
  ".container": {
    display: "flex",
    margin: "1.5rem",
  },
  ".radioGroupContainer": {
    textAlign: "left",
  },
  ".radioBtn": {
    border: "none",
    color: theme.palette.primary.mainText,
  },
  ".radioLabel": {
    width: "38%",
    fontSize: "24px",
    fontWeight: 400,
  },
  ".inputStyle": {
    margin: "0.5rem 4rem",
    marginLeft: "15rem",
  },
  ".policyText": {
    color: theme.palette.primary.main,
    textAlign: "center",
    margin: "5rem",
    fontSize: "22px",
    fontWeight: 400,
  },
  ".captchaContainer1": {
    display: "flex",
    flexDirection: "column",
    width: "15%",
    margin: "4px",
  },
  ".CaptchaInputStyle": {
    margin: "1rem 0px",
    fontSize: "24px",
    border: "1px solid #787878",
    radius: "6px",
  },
  ".captchaLabel": {
    fontSize: "22px",
    fontWeight: 400,
  },
  ".btnStyle": {
    border: `2px solid theme.palette.primary.main`,
    borderRadius: "6px",
    textTransform: "capitalize",
    fontSize: "1.2rem",
  },
  ".captchaValue": {
    width: "15%",
    backgroundColor: "#D9D9D9",
    textAlign: "center",
    padding: "1.5rem",
    margin: "1rem 4px",
  },
}));

const InvestementRiskInfo = ({ formik }) => {
  return (
    <InsvementRiskStyle>
      <Box>
        <Box className="box1">
          <Typography sx={{ fontSize: "28px", fontWeight: 500 }}>
            Investment Risk Profiler
          </Typography>
        </Box>

        <Typography className="heading">
          Welcome to <span style={{ color: "#1EA596" }}> Risk Profiler </span>
          <br />
          Your Investment is utmost as important as understanding your emotional
          risk appetite
        </Typography>

        <div className="container">
          <Typography className="radioLabel">Investment Experience:</Typography>
          <RadioGroup
            name="investment_experience"
            value={formik.values.investment_experience}
            onChange={formik.handleChange}
          >
            <Box className="radioGroupContainer">
              <FormControlLabel
                className="radioBtn"
                value="0-3 yr"
                control={<Radio />}
                label="0-3 yr"
              />
              <FormControlLabel
                className="radioBtn"
                value="3-5 yr"
                control={<Radio />}
                label="3-5 yr"
              />
              <FormControlLabel
                className="radioBtn"
                value="5-7 yr"
                control={<Radio />}
                label="5-7 yr"
              />
              <FormControlLabel
                className="radioBtn"
                value=">7 yr"
                control={<Radio />}
                label=">7 yr"
              />
            </Box>
          </RadioGroup>
        </div>

        <div className="container">
          <Typography className="radioLabel">Investment Style:</Typography>
          <RadioGroup
            name="investment_style"
            value={formik.values.investment_style}
            onChange={formik.handleChange}
          >
            <Box className="radioGroupContainer">
              <FormControlLabel
                className="radioBtn"
                value="Active"
                control={<Radio />}
                label="Active"
              />
              <FormControlLabel
                className="radioBtn"
                value="Passive"
                control={<Radio />}
                label="Passive"
              />
            </Box>
          </RadioGroup>
        </div>

        <div className="container">
          <Typography className="radioLabel">
            How would you react if well diversified portfolio fells below 10%:
          </Typography>
          <RadioGroup
            name="reaction_on_portfolio_diversified"
            value={formik.values.reaction_on_portfolio_diversified}
            onChange={formik.handleChange}
          >
            <Box className="radioGroupContainer">
              <FormControlLabel
                className="radioBtn"
                value="Accumulate"
                control={<Radio />}
                label="Accumulate"
              />
              <FormControlLabel
                className="radioBtn"
                value="Hold"
                control={<Radio />}
                label="Hold"
              />
              <FormControlLabel
                className="radioBtn"
                value="Reduce"
                control={<Radio />}
                label="Reduce"
              />
              <FormControlLabel
                className="radioBtn"
                value="Exit"
                control={<Radio />}
                label="Exit"
              />
            </Box>
          </RadioGroup>
        </div>

        <div className="container">
          <Typography className="radioLabel">Investment Objective</Typography>
          <Select
            fullWidth={true}
            size="medium"
            className="inputStyle"
            name="investment_objective"
            displayEmpty
            value={formik.values.investment_objective}
            onChange={formik.handleChange}
          >
             <MenuItem disabled value="">
              Select
            </MenuItem>
          </Select>
        </div>

        <div className="container">
          <Typography className="radioLabel">Risk Tolerance</Typography>
          <Select
            fullWidth={true}
            displayEmpty
            className="inputStyle"
            name="risk_tolerance"
            value={formik.values.risk_tolerance}
            onChange={formik.handleChange}
          >
             <MenuItem disabled value="">
              Select
            </MenuItem>
          </Select>

        </div>

        <div className="container">
          <Typography className="radioLabel">Investment Horizon:</Typography>
          <RadioGroup
            name="investment_horizon"
            value={formik.values.investment_horizon}
            onChange={formik.handleChange}
          >
            <Box className="radioGroupContainer">
              <FormControlLabel
                className="radioBtn"
                value="<3 yr"
                control={<Radio />}
                label="<3 yr"
              />
              <FormControlLabel
                className="radioBtn"
                value="3-5 yr"
                control={<Radio />}
                label="3-5 yr"
              />
              <FormControlLabel
                className="radioBtn"
                value="5-7 yr"
                control={<Radio />}
                label="5-7 yr"
              />
              <FormControlLabel
                className="radioBtn"
                value=">10 yr"
                control={<Radio />}
                label=">10 yr"
              />
            </Box>
          </RadioGroup>
        </div>

        <Typography className="policyText">
          I / We understand, the above informatio provided is correct to the
          best of my knowledge / belief, and understand the adversities / risk
          tolerance arising from mis - placement of information / data in any
          manner
        </Typography>
        <Box className="captchaValue">
          <Typography className="captchaLabel">Captcha Code</Typography>
        </Box>
        <Box className="captchaContainer1">
          <Typography className="captchaLabel">Enter Captcha</Typography>
          <input type="text" className="CaptchaInputStyle" fullWidth={true} />
          <Button variant="contained" className="btnStyle" type="submit">
            Submit
          </Button>
        </Box>
      </Box>
    </InsvementRiskStyle>
  );
};

export default InvestementRiskInfo;
